.PHONY: tungsten titanium encrypt decrypt reqs forcereqs gitinit

rhodium tungsten titanium lxc-caddy:
	ansible-playbook run.yaml --limit $@ --become $(if $(filter $(shell hostname),$@),--connection local)

titanium-compose:
	ansible-playbook run.yaml --limit titanium --become --tags compose

update:
	ansible-playbook update.yaml --become

encrypt decrypt:
	ansible-vault $@ vars/vault.yaml

reqs:
	ansible-galaxy install -r requirements.yaml

forcereqs:
	ansible-galaxy install -r requirements.yaml --force

terraform-init:
	cd terraform; terraform init

terraform-plan:
	cd terraform; . ./.tf_env; terraform plan

terraform-apply:
	cd terraform; . ./.tf_env; terraform apply

terraform-destroy:
	cd terraform; . ./.tf_env; terraform destroy

gitinit:
	@./git-init.sh
	@echo "ansible vault pre-commit hook installed"
	@echo "don't forget to create a .vault-password too"
