provider "proxmox" {
  pm_api_url = "https://titanium.local.vandermey.ca:8006/api2/json"
  pm_api_token_id = var.proxmox_api_token_id
  pm_api_token_secret = var.proxmox_api_token_secret
}

resource "proxmox_lxc" "caddy" {
  target_node  = "titanium"
  hostname     = "lxc-caddy"
  ostemplate   = "local:vztmpl/debian-11-standard_11.7-1_amd64.tar.zst"
  password     = var.caddy_root_password
  unprivileged = true

  ssh_public_keys = <<-EOT
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICLMSocBi8DCuWtdei5WwEAuMrQsKRBsiGNBtkaqv6gl
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPslMG4y8Xx0bGStu/XbYKjPK7yNTp+ANH4o8cNZKe6s
  EOT

  cores = "1"
  memory = "512"

  features {
    nesting = true
  }

  rootfs {
    storage = "appdata"
    size    = "8G"
  }

  network {
    name   = "eth0"
    bridge = "vmbr0"
    ip     = "192.168.129.1/20"
    gw     = "192.168.128.1"
  }
  nameserver   = "192.168.128.2"
  searchdomain = "local.vandermey.ca"

  start = true
  onboot = true
  vmid   = 200

  # Wait for SSH connection to come up
  provisioner "remote-exec" {
    inline = ["echo 'Connected'"]

    connection {
      type = "ssh"
      host = "${self.hostname}"
      user = "root"
      password = var.caddy_root_password
      private_key = file(var.ssh_private_key)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False make -C .. lxc-caddy"
  }

  # Clean up stale host keys
  provisioner "local-exec" {
    command = "sed -i.bak '/^lxc-caddy/d' ~/.ssh/known_hosts"
    when = destroy
  }
}
