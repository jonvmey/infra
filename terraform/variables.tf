variable "ssh_private_key" {
  type = string
  default = "~/.ssh/id_ed25519"
}
variable "proxmox_api_token_id" {
  type = string
}
variable "proxmox_api_token_secret" {
  type = string
}
variable "caddy_root_password" {
  type = string
}
